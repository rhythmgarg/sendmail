<?php

include_once "/var/www/html/code/config.php";
require('SendMail.php');
require('WriteExcel.php');
date_default_timezone_set('Asia/Kolkata');

$date = date("Y-m-d");
$date = strtotime($date);
$date = strtotime("-1 day", $date);
$date = date('Y-m-d', $date);

$filename = "Description-" . $date . "";
$startdate = $date . ' 00:00:00';
$enddate = $date . ' 23:59:59';

$subject = 'AWB Description Report of DXR';

$sql = "SELECT * FROM `bacode_discraption` WHERE `date_time` BETWEEN '$startdate' AND '$enddate'";

$results = mysql_query($sql);
$totalReceived = mysql_num_rows($result);

if (mysql_num_rows($results) > 0) {
    $rowDataF = array();
    while ($row = mysql_fetch_assoc($results)) {
        $rowData = array();
        $rowData['awb_no'] = $row['awb_no'];
        $rowData['discraption'] = $row['discraption'];
        $rowData['date_time'] = $row['date_time'];
        $rowData['status'] = $row['status'];
        $rowDataF[] = $rowData;
    }


    $body = "Total Received AWD Description: $totalReceived\r\n";
    $sendmail = new SendMail;
    $recipient = ["email" => "rhythmgarg18@gmail.com", "name" => "Rhythm"];
    // $recipient = ["email" => "dhirenderd@ecomexpress.in", "name" => "Dhirender"];
    // $cc = [
    //     ["email" => "manoj.singh@ecomexpress.in", "name" => "Manoj"],
    //     ["email" => "pankajshishodia@ecomexpress.in", "name" => "Pankaj"]
    // ];
    $subject = 'Description X-Ray Scanning Report of DXR';
    $filename = 'Description-' . $date;
    $path = WriteExcel::writeDescription($rowDataF, $filename);
    $attachments = [
        [
            "path" => $path,
            "name" => $filename . '.xls',
        ]
    ];
    $sendmail->setRecipient($recipient)
        ->subject($subject)
        ->body($body)
        ->attachments($attachments)->setCC($cc)
        ->send();
                // ->setBcc($bcc)
                // 

} else {
    echo 'No data Found!';
}

?>
