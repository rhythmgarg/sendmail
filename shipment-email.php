<?php
include_once "/var/www/html/code/config.php";
require('SendMail.php');
require('WriteExcel.php');
date_default_timezone_set('Asia/Kolkata');

$date = date("Y-m-d");
$date = strtotime($date);
$date = strtotime("-1 day", $date);
$date = date('Y-m-d', $date);

$filename = 'Shipment-' . $date;
$startdate = $date . " 00:00:00";
$enddate = $date . " 23:59:59";
$body = '';

$sql = "select `u`.`empid`, `b`.`basket`, `b`.`awb`, `b`.`time`, `b`.`date`, `bi`.`image`, `bi`.`imgstatus`, `bs`.`status`, `b`.`flag`, `b`.`discraption` from barcode_detail as b left join user_data as u on (b.user=u.empId)
    LEFT JOIN bacode_status as bs on (b.basket=bs.basket_no),basketimage bi where bi.basket=b.basket and b.datetime BETWEEN '$startdate' AND '$enddate'";

$results = mysql_query($sql);
$totalshipment = mysql_num_rows($results);

if (mysql_num_rows($results) > 0) {
    $rowDataF = array();
    while ($row = mysql_fetch_assoc($results)) {
        $rowData = array();
        $rowData['empid'] = $row['empid'];
        $rowData['basket'] = $row['basket'];
        $rowData['awb'] = $row['awb'];
        $rowData['time'] = $row['time'];
        $rowData['date'] = $row['date'];
        $rowData['image'] = $row['image'];
        $rowData['status'] = $row['status'];
        $rowData['flag'] = $row['flag'] ? $row['flag'] : null;
        $rowData['discraption'] = $row['discraption'];
        $rowData['imgstatus'] = ($row['imgstatus'] == '1') ? 'Done' : 'Not Done';

        $rowDataF[] = $rowData;
    }
    $body = "Total Shipment: $totalshipment\r\n";
    $sendmail = new SendMail;
    $recipient = ["email" => "rhythmgarg18@gmail.com", "name" => "Rhythm"];
    // $recipient = ["email" => "dhirenderd@ecomexpress.in", "name" => "Dhirender"];
    // $cc = [
    //     ["email" => "manoj.singh@ecomexpress.in", "name" => "Manoj"],
    //     ["email" => "pankajshishodia@ecomexpress.in", "name" => "Pankaj"]
    // ];
    $subject = 'Shipment X-Ray Scanning Report of DXR';
    $filename = 'Shipment-' . $date;
    $path = WriteExcel::writeShipment($rowDataF, $filename);
    $attachments = [
        [
            "path" => $path,
            "name" => $filename . '.xls',
        ]
    ];
    $sendmail->setRecipient($recipient)
        ->subject($subject)
        ->body($body)
        ->attachments($attachments)
        ->send();
            // ->setBcc($bcc)
            // ->setCC($cc)

} else {
    echo 'No data Found!';
}


?>
