<?php

require 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class SendMail
{

    public $mail;

    private $recipient = null;

    private $host = 'smtp.gmail.com;';                    // Specify main and backup SMTP servers
    private $username = 'isoftcaredevlop@gmail.com';          // SMTP username
    private $from = 'isoftcaredevlop@gmail.com';
    private $password = 'isoftcaredevlop@123';                // SMTP password
    private $port = 587;                                  // TCP port to connect to
    private $name = 'Mailer';                             // Mailer Name
    private $reply_to = 'isoftcaredevlop@gmail.com';          // replyTo Address
    private $reply_to_name = 'Information';                   // replyTo Name
    
    public function __construct()
    {
        $this->mail = new PHPMailer(true);
        $this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $this->mail->isSMTP();                                      // Set mailer to use SMTP
        $this->mail->Host = $this->host;
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = $this->username;
        $this->mail->Password = $this->password;
        $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = $this->port;
        $this->mail->setFrom($this->from, $this->name);
        $this->mail->addReplyTo($this->reply_to, $this->reply_to_name);
        $this->mail->isHTML(true);                                  // Set email format to HTML
    }



    /**
     * @param $name
     *
     * @return  string
     */
    public function send()
    {

        try {
            
            $this->mail->send();

            echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }
    }


    /**
     * Set the value of recipient
     *
     * @return  self
     */
    public function setRecipient($recipient = null)
    {
        $this->recipient = $recipient;
        
        if (!is_array($this->recipient))
            $this->mail->addAddress($this->recipient);
        else if (isset($this->recipient['email']) && isset($this->recipient['name']))
            $this->mail->addAddress($this->recipient['email'], $this->recipient['name']);
        else if (isset($this->recipient['email']) && !isset($this->recipient['name']))
            $this->mail->addAddress($this->recipient['email']);
        else
            throw new Exception("Reciepient email is not set", 1);

        return $this;
    }

    /**
     * Set the value of subject
     *
     * @return  self
     */
    public function subject($subject = null)
    {
        $this->mail->Subject = $subject;

        return $this;
    }

    /**
     * Set the value of body
     *
     * @return  self
     */
    public function body($body = null)
    {
        $this->mail->Body = $body;

        return $this;
    }



    /**
     * Set the attachments
     *
     * @return  self
     */
    public function attachments($attachments)
    {
        $attachments = (array)$attachments;

        foreach ($attachments as $attachment) {
            if (!is_array($attachment))
                $this->mail->addAttachment($attachment);
            else if (isset($attachment['path']) && isset($attachment['name']))
                $this->mail->addAttachment($attachment['path'], $attachment['name']);
            else if (isset($attachment['path']) && !isset($attachment['name']))
                $this->mail->addAttachment($attachment['path']);
        }

        return $this;
    }

    /**
     * Set the value of cc
     *
     * @return  self
     */
    public function setCc($ccs)
    {
        $ccs = (array)$ccs;

        foreach ($ccs as $cc) {
            if (!is_array($cc))
                $this->mail->addCC($cc);
            else if (isset($cc['email']) && isset($cc['name']))
                $this->mail->addCC($cc['email'], $cc['name']);
            else if (isset($cc['email']) && !isset($cc['name']))
                $this->mail->addCC($cc['email']);
        }

        return $this;
    }

    /**
     * Set the value of cc
     *
     * @return  self
     */
    public function setBcc($bccs)
    {
        $bccs = (array)$bccs;

        foreach ($bccs as $bcc) {

            if (!is_array($bcc))
                $this->mail->addBCC($bcc);
            else if (isset($bcc['email']) && isset($bcc['name']))
                $this->mail->addBCC($bcc['email'], $bcc['name']);
            else if (isset($bcc['email']) && !isset($bcc['name']))
                $this->mail->addBCC($bcc['email']);

        }

        return $this;
    }
}
