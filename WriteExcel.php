<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class WriteExcel
{
    public static function writeShipment($results, $filename)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("A1", "Emp-ID");
        $sheet->setCellValue("B1", "Location");
        $sheet->setCellValue("C1", "Basket");
        $sheet->setCellValue("D1", "A.W.B");
        $sheet->setCellValue("E1", "Time");
        $sheet->setCellValue("F1", "Date");
        $sheet->setCellValue("G1", "X-Ray Image");
        $sheet->setCellValue("H1", "Product Image");
        $sheet->setCellValue("I1", "Remarks");
        $sheet->setCellValue("J1", "Flag");
        $sheet->setCellValue("K1", "Description");
        $sheet->setCellValue("L1", "Image Upload Status");


        $row = 2;

        foreach ($results as $result) {
            $sheet->setCellValueByColumnAndRow(1, $row, $result['empid']);
            $sheet->setCellValueByColumnAndRow(2, $row, 'DXR');
            $sheet->setCellValueByColumnAndRow(3, $row, $result['basket']);
            $sheet->setCellValueByColumnAndRow(4, $row, $result['awb']);
            $sheet->setCellValueByColumnAndRow(5, $row, $result['time']);
            $sheet->setCellValueByColumnAndRow(6, $row, $result['date']);
            $sheet->setCellValueByColumnAndRow(7, $row, $result['image']);
            $sheet->setCellValueByColumnAndRow(8, $row, $result['awb'] . '_' . $result['basket'] . '.jpg');
            $sheet->setCellValueByColumnAndRow(9, $row, $result['status']);
            $sheet->setCellValueByColumnAndRow(10, $row, $result['flag']);
            $sheet->setCellValueByColumnAndRow(11, $row, $result['discraption']);
            $sheet->setCellValueByColumnAndRow(12, $row, $result['imgstatus']);
            ++$row;

        }
        $writer = new Xls($spreadsheet);
        $fileDirectory = realpath(__DIR__ . "/tmp");
        $finalExcelocation = $fileDirectory . '/' . $filename . '.xls';
        $writer->save($finalExcelocation);
        return $finalExcelocation;
    }

    public static function writeDescription($results, $filename)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("A1", "Location");
        $sheet->setCellValue("B1", "A.W.B");
        $sheet->setCellValue("C1", "Description");
        $sheet->setCellValue("D1", "InScanDateTime");
        $sheet->setCellValue("E1", "Status");

        $row = 2;

        foreach ($results as $result) {
            $sheet->setCellValueByColumnAndRow(1, $row, 'DXR');
            $sheet->setCellValueByColumnAndRow(2, $row, $result['awb_no']);
            $sheet->setCellValueByColumnAndRow(3, $row, $result['discraption']);
            $sheet->setCellValueByColumnAndRow(4, $row, $result['date_time']);
            $sheet->setCellValueByColumnAndRow(5, $row, $result['status']);
            ++$row;
        }
        $writer = new Xls($spreadsheet);
        $fileDirectory = realpath(__DIR__ . "/tmp");
        $finalExcelocation = $fileDirectory . '/' . $filename . '.xls';
        $writer->save($finalExcelocation);
        return $finalExcelocation;
    }


}
